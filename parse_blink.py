import matplotlib.pyplot as plt
import scipy.signal as signal
from scipy.signal import butter, lfilter
import numpy as np


fil = "/home/zack/Documents/jenn_eye_blink_200uv.txt"
sample = open(fil, "r")
voltage_list = []
time_list = []
for line in sample.readlines():
    if line[0:1] == '%':
        continue
    else:
        column = line.split(", ")
        t = column[0]
        v = column[2]
        time_list.append(t)
        voltage_list.append(v)

print len(voltage_list)
sample.close()

highcut = 50       # Highpass frequency in Hz
lowcut = 1      # Cutoff low frequency in Hz
fs = 250

T = 60                         # time in seconds
nsamples = T * fs
t = np.linspace(0, T, nsamples, endpoint=False)

nyq = 0.5 * fs
low = lowcut / nyq
high = highcut / nyq

plt.plot(voltage_list[0:7694],'r-')
#plt.ylabel('uv')
plt.show()
